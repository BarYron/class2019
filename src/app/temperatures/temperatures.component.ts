import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

  likes:number = 0;

  addlikes(){
    this.likes++
  }
  
  constructor() { }

  ngOnInit() {
  }

}
